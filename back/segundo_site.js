var request = require('request');
var cheerio = require('cheerio');
const fs = require('fs');
 

function scrap(url) {
    request(url, parseRequest);
}

function parseRequest(error, response, html) {
    if (error){
        console.log(error)
    };
    // var com o conteudo total da pagina
    let $ = cheerio.load(html);
    // div contendo a lista de filmes
    let filmes = $('.item');

    let moview = []
    // loop por cada div que contem card de filmes
    filmes.each(function (i, e) {
        let imagens = $(this).children('div[class=image_content]').children().first().children().attr('data-src');          
        let titulo = $(this).children('div[class=info]').children().children('div[class=flex]').children().attr('title');      
        let getNota = $(this).children('div[class=info]').children().children().children('div[class=outer_ring]').children().attr('data-percent');
        let avaliacao = getNota.slice(0,2);
        moview.push({
            id: i,
            imagens,
            titulo,
            avaliacao
        })
    });

    
    let Data = {};
  
    Data = {
        themoviedb: moview
    }

    const dataForm = JSON.stringify(Data)
    
    fs.writeFile("data.json", dataForm, 'utf8', function (err) {
    if (err) {
        console.log("Ops erro.");
        return console.log(err);
    }
 
    console.log("JSON criado com sucesso");
    });

}

var url = "https://www.themoviedb.org/movie/top-rated";
scrap(url);